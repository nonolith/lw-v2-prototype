# Implements module loading
fs = require 'fs'
path = require 'path'
nodes = require './nodes'

EXT = ".lang"
MODULEDIR = path.resolve(__dirname, '../lib')

# Get the filename of a core module
moduleLibName = (name) -> path.resolve(MODULEDIR, "#{name}#{EXT}")

# Cache of module ASTs by name
loadedModules = {}

# Load a module by filename
@loadFile = loadFile = (fname) ->
	data = fs.readFileSync(fname, 'utf8')
	tree = nodes.parse(fname, data)
	tree.resolve()
	return tree

# Load a module by name
@load = load = (name) ->
	loadedModules[name] ?= loadFile(moduleLibName(name))