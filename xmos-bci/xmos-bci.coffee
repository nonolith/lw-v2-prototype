usb = require 'usb'

{GenerateExec, Exec} = require '../exec'
{ASTtoFA, endStep, RepeatCountStep} = require '../compile'
{EntityPrimitive, RepeatContext} = require '../context'

{load} = require '../module'
nodes = require '../nodes'

BC =
	HALT:        0b00000000
	JUMP_U:      0b00000100
	WAIT_U:      0b00001000
	WAIT_C:      0b00001001
	WAIT_PIN_UU: 0b00100000
	IN_U:        0b10010000
	OUT_U:       0b11010000
	OUT_C:       0b11000000

TIMER_SCALE = 1e8

VID = 0x9999
PID = 0xFFFF

compileOnly = false

symH = nodes.SymbolValue.create('h')
symL = nodes.SymbolValue.create('l')

encodeLevel = (s) -> +(s == symH)
decodeLevel = (s) -> if s then symH else symL

defaultRule =  (n) ->
	@walk n, defaultRules

wireDef = load('Wire').entityDefs.Wire
deviceDef = load('Wire').entityDefs.ValueDump

console.assert wireDef and deviceDef

class BCIPrimitive extends EntityPrimitive

@Device = class BCIDevice extends EntityPrimitive
	constructor: ->
		super(deviceDef)

		@wires = {}
		for i in [0...8] then @wires["p#{i}"] = new BCIWire(this, i)

		@outBuffer = []
		@inBuffer = []
		@inWaiting = []

		@rxBytes = 0
		@txBytes = 0

		unless compileOnly
			[@usbDev] = usb.find_by_vid_and_pid(VID, PID)
			console.assert @usbDev, "Device not found"
			@usbDev.timeout = 1000*100
			@iface = @usbDev.interface(0)
			[@inEp, @outEp] = @iface.endpoints
			console.assert @inEp and @outEp

	ASTtoFA: (context, startNode) ->
		console.log "Compiling with BCI backend"
		start = new SyncStep(context, this, false)
		end = new SyncStep(context, this, true)
		
		{start:start.next} = ASTtoFA context, startNode, end,
			device: this
			repeatStepCount: BCIRepeatStepCount
			repeatStepParse: (context, node) -> node.error("Backend does not support loops without defined count")
		

		end.next = endStep
		return {start, end}

	getSubEntity: (name) -> @wires[name]

	prepare: (start) ->
		bytecode = []
		bytecode.compile = (start) ->
			while start
				if start.compile #instanceof BCIStep
					start.address = @length
					v = start.compile(@)
					start.nextAddress = @length
				start = start.next

		bytecode.pushOp32 = (op, n) ->
			@push op, n&0xff, (n>>8)&0xff, (n>>16)&0xff, (n>>24)&0xff

		bytecode.compile(start)

		console.log 'bytecode:', bytecode.length, ("0x#{x.toString(16)}" for x in bytecode).join(', ')
		throw "exit" if compileOnly

		@iface.claim()

		@send16(bytecode.length)
		@sendArray(bytecode)
		@flushOut()

		@inEp.startStream 2, 512
		@inEp.on 'data', (d) =>
			#console.log "received", d
			@rxBytes += d.length
			@inBuffer.push i for i in d
			oldWaiting = @inWaiting
			@inWaiting = []
			i() for i in oldWaiting
			null

		@inEp.on 'error', (e) ->
			console.log "IN error", e unless e is 3 # cancelled

		#@inEp.on 'end', (e) ->
		#	console.log "End of IN endpoint"


	after: ->
		console.log "inBuffer still has #{@inBuffer.length}" if @inBuffer.length
		console.log "outBuffer still has #{@outBuffer.length}" if @outBuffer.length
		console.log "TX: #{@txBytes} bytes, RX: #{@rxBytes} bytes"
		@inEp.stopStream()

	sendByte: (x) ->
		console.assert x?
		@outBuffer.push x
		#console.log 'sendB', x

	send16: (x) ->
		console.assert x?
		@outBuffer.push x&0xff, (x>>8)&0xff
		#console.log 'send16', x

	sendUint: (x) ->
		console.assert x?
		@outBuffer.push x&0xff, (x>>8)&0xff, (x>>16)&0xff, (x>>24)
		#console.log 'sendU', x

	sendArray: (x) ->
		@outBuffer = @outBuffer.concat(x)

	flushOut: ->
		#console.log 'sending', ("0x#{x.toString(16)}" for x in @outBuffer).join(', '), @outBuffer.length
		@txBytes += @outBuffer.length
		@outEp.transfer new Buffer(@outBuffer), (d, e) ->
			#console.log "sent", d, e
		@outBuffer = []

	readByte: (exec, cb) ->
		# TODO: not compatible with backtracking
		f = =>
			if @inBuffer.length >= 1
				cb(@inBuffer.shift())
			else
				@inWaiting.push(f)
		f()

	readUint: (exec, cb) ->
		# TODO: not compatible with backtracking
		f = =>
			if @inBuffer.length >= 4
				[b1,b2,b3,b4] = @inBuffer.splice(0, 4)
				cb(b4<<24 | b3<<16 | b2<<8 | b1)
			else
				@inWaiting.push(f)
		f()


class BCIWire extends BCIPrimitive
	constructor: (@device, @id) ->
		super(wireDef)

	toFA: (context, event, args, timer) ->
		switch @values.direction.sym
			when 'in'
				switch event
					when 'level'
						new WireLevelInStep(context, @device, this, args.type, timer)
					when 'edge'
						new WireEdgeInStep(context, @device, this, args.type, timer)
					else
						throw new Error("BCIWire has no event #{event}!")
			when 'out'
				switch event
					when 'level', 'edge'
						new WireLevelOutStep(context, @device, this, args.type, timer)
					else
						throw new Error("BCIWire has no event #{event}!")
			else
				throw new Error("Bidirectional wires unsupported")
			

	getDeviceSet: -> [@device]

class BCIStep
	# Up (if `v` is present) or down (if not) evaluate a timer, converting to
	# device timer units
	timerDown: (exec) ->
		return 0 if not @timer
		c = if exec then exec.lastTimerContext else @timer.context
		v = @timer.expr.evalDown(c)
		return null if v instanceof nodes.AbstractValue
		return 0 if v is nodes.ignore
		console.assert v instanceof nodes.NumberValue, "number was a #{v}"
		Math.floor(v.num*TIMER_SCALE)

	timerUp: (exec, v) ->
		if @timer
			c = if exec then exec.lastTimerContext else @timer.context
			console.assert(c)
			@timer.expr.evalUp(c, new nodes.NumberValue(v/TIMER_SCALE))

class SyncStep extends BCIStep
	constructor: (@context, @dev, @end) ->
	compile: (b) -> b.push BC.HALT
	runGenerate: (exec) ->
		if @end
			@dev.send16(0) # jump to 0 shuts down BCI thread
		@dev.flushOut()
		null

	run: (exec) ->
		if not @end
			@dev.send16(@nextAddress)
			exec.cloneGenerate().run(@next)
		@dev.readByte exec, (b) =>
			throw new Error "Sync reply got 0x#{b.toString(16)}" if b != 0x55
			exec.run @next

class WireLevelOutStep extends BCIStep
	constructor: (@context, @dev, @wire, @levelArg, @timer) ->
		@staticValue = @levelArg.evalDown(@context)
		@constant = @staticValue instanceof nodes.SymbolValue
		@timerConstant = @timerDown()

	compile: (b) ->
		if @timerConstant isnt null
			if @timerConstant != 0
				b.pushOp32 BC.WAIT_C, @timerConstant
		else
			b.push BC.WAIT_U

		if @constant
			b.push BC.OUT_C|encodeLevel(@staticValue)<<3|@wire.id
		else unless @staticValue is nodes.ignore
			b.push BC.OUT_U|@wire.id
			
	runGenerate: (exec) ->
		if @timerConstant is null
			@dev.sendUint @timerDown(exec)

		unless @constant or @staticValue is nodes.ignore
			l = @levelArg.evalDown(exec.context)
			@dev.sendByte encodeLevel(l)

		exec.run @next

	run: (exec) ->
		exec.run @next

class WireLevelInStep extends BCIStep
	constructor: (@context, @dev, @wire, @levelArg, @timer) ->
		@timerConstant = @timerDown()

	compile: (b) ->
		if @timerConstant isnt null
			if @timerConstant != 0
				b.pushOp32 BC.WAIT_C, @timerConstant
		else
			b.push BC.WAIT_U

		b.push BC.IN_U|@wire.id

	runGenerate: (exec) ->
		if @timerConstant is null
			@dev.sendUint @timerDown(exec)
		exec.run @next

	run: (exec) ->
		@dev.readByte exec, (b) =>
			@levelArg.evalUp(exec.context, decodeLevel(b))
			exec.run @next

class WireEdgeInStep extends BCIStep
	constructor: (@context, @dev, @wire, @levelArg, @timer) ->

	compile: (b)->
		b.push BC.WAIT_PIN_UU|@wire.id

	runGenerate: (exec) ->
		@dev.sendByte encodeLevel(@levelArg.evalDown(exec.context))
		exec.run @next

	run: (exec) ->
		@dev.readUint exec, (v) =>
			@timerUp(exec, v)
			exec.run @next

class BCIRepeatStepCount extends RepeatCountStep
	compile: (b) ->
		@insideAddress = b.length
		b.compile @inside
		b.push BC.JUMP_U
		@afterAddress = b.length
		b.compile @until

	onIterate: (exec) ->
		if exec.isGenerate then @dev.send16 @insideAddress

	onDone: (exec) ->
		if exec.isGenerate then @dev.send16 @afterAddress
