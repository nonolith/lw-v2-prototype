Usage
-----

	git clone --recursive git@bitbucket.org:nonolith/lw-v2-prototype.git lw
	cd lw
	npm install
	npm test
	# set XMOS tools path

	make -C xmos-bci/firmware/ load 
	./run_script.coffee lib/SPI.lang d=x:usbdevice do=10101110 di=\?

Pinout
------

	0 XS1_PORT_1L TRST_N
	1 XS1_PORT_1A TDSRC
	2 XS1_PORT_1C TMS
	3 XS1_PORT_1D TCK
	4 XS1_PORT_1K DEBUG
	5 XS1_PORT_1B TDSNK
	6 XS1_PORT_1M RST_N
	7 XS1_PORT_1J UART_RX

	  / v                                     /
	 +---------------------------------------+
	 | 5V  p0  p1  p2  p3  p4  p5  p6  p7  X |
	 | X   G   X   G   X   G   X   G   X   G | /
	 +---------------------------------------+

Caveats
-------

  * If the host software crashes, the device won't recover. Reboot it with `make load`.
  * Initial values are not used -- pins become output when set for the first time
  * Pins are not reset to high-z at the end of the program
  * It will randomly crash and stuff