exec  = require './exec'
nodes = require './nodes'
{load} = require './module'
{EntityPrimitive} = require './context'

# Module implementing wire primitives that source data from or generate
# static representations of a series of wire states.

symH = nodes.SymbolValue.create('h')
symL = nodes.SymbolValue.create('l')

symToBool = (s) -> s == symH
boolToSym = (s) -> if s then symH else symL

wireDef = load('Wire').entityDefs.Wire
@def = dumpDef = load('Wire').entityDefs.ValueDump

console.assert wireDef and dumpDef

# Abstract base class for the main device primitive
class Dump extends EntityPrimitive
	constructor: ->
		super(dumpDef)

@Write = class DumpWrite extends Dump
	constructor: (@format, @fname) ->
		super()

		# WriteWire instances by name
		@wires = {}

		# {time, id, value} objects, must be in time order
		@data = []

	getSubEntity: (name) ->
		@wires[name] ?= new WriteWire(name, @)

	get: -> @format.generate(@wires, @data)

class WriteWire extends EntityPrimitive
	constructor: (@id, @parent) ->
		super(wireDef)

	value: (time, value, msg) ->
		if value and value != nodes.ignore
			@parent.data.push {time, @id, value}
		#console.log("#{@id}: #{msg} #{value} @ #{time}")

	handlers:
		edge: (exec) ->
			exec.incTime  @time(exec)
			@entity.value(exec.time, @arg(exec, 'type'), 'Edge')

		level: (exec) ->
			exec.incTime  @time(exec)
			@entity.value(exec.time, @arg(exec, 'type'), 'Level')

	getDeviceSet: -> [@parent]


@Read = class DumpRead extends Dump
	constructor: (@format, str) ->
		super()
		@wires = @format.parse(this, str)

	getSubEntity: (name) -> @wires[name]

class ReadWire extends EntityPrimitive
	constructor: (@parent, @id) ->
		super(wireDef)

		# {time, value}, sorted by time
		@values = []

	handlers:
		edge: (exec) ->
			edge = @arg(exec, 'type')

			v = symL
			t = Infinity

			for i in @entity.values
				v = i.value
				if i.time >= exec.time and v == edge
					t = i.time
					break

			#console.log(@entity.id, 'edge', t, edge)
			@time(exec, t-exec.time)
			exec.time = t

		level: (exec) ->
			exec.incTime  @time(exec)
			v = symL
			for i in @entity.values
				if i.time > exec.time then break
				v = i.value

			#console.log(@entity.id, 'level', exec.time, v)

			@arg(exec, 'type', v)

	getDeviceSet: -> [@parent]



# Formatters are used to convert between textual representations and the
# internal data structures.

# The simple format is a human readable/writable representation, used mostly
# for unit tests.
# 
# It's just lines of the format "1: w = #h" to say that at time=1,
# the wire `w` is high

@SimpleFormat = 
	parse: (parent, text) ->
		wires = {}

		get = (id) ->
			wires[id] ?= new ReadWire(parent, id)

		for line in text.split(/\n/)
			m = /(\d+):\s*(\w+)\s*=\s*\$([hl])/.exec(line)

			if m
				time = parseFloat(m[1],10)
				id = m[2]
				value = boolToSym(m[3]=='h')
				get(id).values.push {time, value}
			else if not /^(\s*$|#)/.test(line)
				throw new Error("SimpleFormat can't parse `#{line}`")
		
		wires

	generate: (wires, values) ->
		lines = []

		put = (time, id, val) -> lines.push "#{time}: #{id} = #{val}"

		for id, w of wires
			put(0, w.id, w.get('initial').expr)

		for {time, id, value} in values
			put(time, id, value)

		lines.join('\n')


# VCD is a standard format for logic analyzers and HDL simulators.
# This implements just enough of VCD to get by.
@VCDFormat = 
	parse: (parent, text) ->
		wires = {}
		wiresVCD = {}

		lines = text.split(/\n/)
		time = -1
		for line in lines
			if r=/\$var wire 1 (.) (\w+) \$end/.exec(line)
				wires[r[2]] = wiresVCD[r[1]] = new ReadWire(parent, r[2])
			else if /\$enddefinitions \$end/.test(line)
				time = 0
				continue

			if time >= 0
				if line[0] == '#'
					time = parseInt(line.slice(1), 10)/1e9
				else
					wiresVCD[line[1]].values.push {time, value:boolToSym(line[0] == '1')}
	
		return wires

	generate: (wires, values) ->
		lines = []

		vcdIds = ['a','b','c','d','e','f']
		idsMap = {}

		for name of wires
			idsMap[name] = vcdIds.shift()

		lines.push """
					$timescale 1 ns $end
					$scope module top $end
					"""

		for id, w of wires
			lines.push "$var wire 1 #{idsMap[w.id]} #{w.id} $end"

		lines.push """
		           $upscope $end
		           $enddefinitions $end
		           #0
		           """
		for id, w of wires
			lines.push "#{+symToBool(w.get('initial').expr)}#{idsMap[id]}"

		for {time, id, value} in values
			lines.push "##{Math.floor(time*1e9)}\n#{+symToBool(value)}#{idsMap[id]}"

		lines.join('\n')
