# Compilation of ASTs to finite automata.

{isValue, topType} = nodes = require './nodes'
{Context, CallContext, RepeatContext, EntityPrimitive} = require './context'
units = require './units'

ASTtoFA = @ASTtoFA = (context, startNode, endNode=endStep, opts=defaultOpts, timer=null) ->
	start = end = {next:null}

	append = (step) ->
		end = (end.next = step)
		step

	rules = 
		SeqBlock: (n) ->
			walk(stmt) for stmt in n.body
			null

		RepeatBlock: (n) ->
			lc = new RepeatContext(context, n)
			c = n.countExpr?.evalDown(context, null)
			rs = append if c and c isnt nodes.ignore
				new opts.repeatStepCount(lc, n, timer, opts)	
			else if n.untilBody
				new opts.repeatStepParse(lc, n, timer, opts)
			else
				n.error 'Repeat has no count or until block'
			timer = null

		EventTemplate: (n) ->
			# Uses the EntityInst to do the work so it generates an EventStep
			# if the entity is a primitive or an EnterScope / sequence /
			# LeaveScope chain if it is user-defined.
			{expr:entityExpr, context:c} = v = context.get(n.entityVarName)
			console.assert entityExpr, n.entityVarName
			entityInst = entityExpr.evalDown(context)
			def = entityInst.entityDef.eventDefs[n.event]
			context = new entityInst.callContext((oldContext = context), entityInst, n, def)

			if entityInst instanceof EntityPrimitive
				append entityInst.toFA(oldContext, n.event, n.args, timer)
				timer = null
			else
				# Wrap the steps of the body sequence with enter/leave scope
				append new EnterScopeStep(context)
				walk(def.body)
				append new LeaveScopeStep(context)
			context = oldContext

		TimeTemplate: (n) ->
			n.error("Duplicate time step") if timer
			t = n.expr.typecheck(context)
			nodes.typecheckTime(t, n)
			append new TimerStep()
			timer = {expr:n.expr, context}

	walk = (astNode) ->
		tp = astNode.constructor.name
		rule = rules[tp] or throw new Error("No rule to handle #{tp} node")
		rule(astNode)

	walk(startNode)
	end.next = endNode
	return {start:start.next, end, timer}

# **Step** is the base class for the finite automata representation. Action
# happens on the transitions in finite automata, so these instances represent
# edges of the state graph.
class Step
	runGenerate: (exec) -> exec.run @next

# Special step that marks the end of the automata.
class EndStep extends Step
@endStep = endStep = new EndStep()

# Look up a handler for a primitive event and make an `EventStep` for it.
EntityPrimitive::toFA = (context, eventName, argTemplate, timer) ->
	fn = @handlers[eventName]
	console.assert(fn)
	console.assert(argTemplate)
	new EventStep(context, this, eventName, fn, argTemplate, timer)

# An **EventStep** generates / parses a primitive entity whose implementation
# is in Javascript. It provides helper methods for those implementations.
class EventStep extends Step
	constructor: (@context, @entity, @eventName, @rfn, @argTemplate, @timer) ->
		@next = null

	# Up/down evaluate an argument to the event. If `v` is *not* passed, down-
	# evaluates and gets the value of argument `k`. If `v` is passed, up-
	# evaluate `v` onto the `k` argument. This takes a context, but `k` is the
	# name of an argument to the primitive event (which does not get its own
	# context), not a context variable name.
	arg: (exec, k, v) ->
		#TODO: should this check type / defaults against original overridden definition?
		if not v
			@argTemplate[k].evalDown(exec.context)
		else
			@argTemplate[k].evalUp(exec.context, v)
			# TODO: handle failure

	# Up/down evaluates the time expression associated with the event. If there
	# is no time expression, gets 0, and ignores being set.
	time: (exec, v) ->
		return 0 if not @timer
		console.assert exec.lastTimerContext

		if not v? # generate
			# TODO: find runtime context for the timer
			@timer.expr.evalDown(exec.lastTimerContext).num or 0
		else # parse
			@timer.expr.evalUp(exec.lastTimerContext, new nodes.NumberValue(v))

	run: (exec) ->
		@rfn.call(this, exec)
		exec.run @next

# Virtual step to capture the runtime context for time expressions
class TimerStep extends Step
	run: (exec) ->
		exec.lastTimerContext = exec.context
		exec.run @next

	runGenerate: @::run

# Virtual step to initialize the context upon entering an event scope.
class EnterScopeStep extends Step
	constructor: (@context) ->

	run: (exec) ->
		@context.enter(exec)
		exec.run @next

	runGenerate: @::run

# Virtual step upon leaving an event scope.
class LeaveScopeStep extends Step
	constructor: (@context) ->

	run: (exec) ->
		@context.leave(exec)
		# TODO: handle failure
		exec.run @next

	runGenerate: @::run

# Virtual step for a repeat block
class RepeatStep extends Step
	constructor: (@context, @repeatDef, @timerBefore, opts) ->
		@dev = opts.device

		timer = if @timerBefore or @repeatDef.body.endsWithTimer()
			{expr:new RepeatContext.TimerExpr(@timerBefore.expr), @context}
		else
			null

		{start:@inside, timer:atEnd} = ASTtoFA(
			@context, @repeatDef.body, undefined, opts, timer)
		timer.expr.atEnd = atEnd.expr if atEnd

		@until = if @repeatDef.untilBody
			ASTtoFA(@context, @repeatDef.untilBody, undefined, opts, timer).start
		else
			endStep

	run: (exec) ->
		insideExec = exec.clone()
		insideExec.parent = exec
		ctx = @context.enter(insideExec)
		insideExec.lastTimerContext = ctx
		@start(ctx, insideExec)
		insideExec.run @inside, @afterIteration

	runGenerate: @::run

	afterIteration: (insideExec) =>
		exec = insideExec.parent
		done = @isDone(insideExec.context, insideExec)

		if done != -1
			exec.restore(insideExec)

		if not done
			insideExec.context.i++
			@onIterate(insideExec)
			insideExec.lastTimerContext = insideExec.context
			insideExec.run @inside
		else
			@context.leave(insideExec)
			@onDone(insideExec)

			oldDone = exec.done # TODO: better .run() API?
			exec.run @until, =>
				exec.done = oldDone
				exec.run @next

	onIterate: ->
	onDone: ->

@RepeatCountStep = class RepeatCountStep extends RepeatStep
	start: (state, insideExec) ->
		# count is set by context.onEnter()

	isDone: (state) ->
		#console.log 'isDone', state.i, state.count
		+(state.i+1 >= state.count)
		
@RepeatParseStep =  class RepeatParseStep extends RepeatStep
	start: (state, insideExec) ->
		state.untilTime = null
		untilExec = insideExec.clone()
		untilExec.run @until, =>
			# TODO: should this be the match time of the first step instead?
			state.untilTime = untilExec.time

	isDone: (state, insideExec) -> 
		if insideExec.time >= state.untilTime
			state.i-- if insideExec.time > state.untilTime
			-1

defaultOpts = 
	device: null
	repeatStepCount: RepeatCountStep
	repeatStepParse: RepeatParseStep