_ = require('underscore')
{BitArray} = require('./bitarray')
moduleLoader = require('./module')
units = require("./units")

exports.parse = (fname, input, startRule='start') ->
	lang = require('./lang')
	try
		lang.parse(input, {startRule, fname})
	catch e
		if e instanceof lang.SyntaxError
			throw new Error("Syntax error at #{fname}:#{e.line}:#{e.column}:\n\t#{e.message}")
		throw e

# Base class of all AST nodes
class ASTNode
	constructor: ->
		@startPos = @text = @input = @file = null # added by parser

	error: (msg) ->
		throw new LangError "#{@file}:#{@lineNumber()}", msg

	warn: (msg) ->
		console.warn("Warning: #{@file}:#{@lineNumber()}: #{msg}")

	sourceString: -> @text

	lineNumber: ->
		if @input
			@input.substring(0, @startPos).split("\n").length
		else ''

	toString: -> "<AST: #{@constructor.name}>"

class Scope extends ASTNode
	constructor: ->
		@vars = {}

	resolve: (@parentScope) ->
		for varName, varDef of @vars
			if varDef instanceof NewDef and not @allowNew
				@error "`new` declaration not allowed here"
			varDef.resolve(this)

	getVarDef: (varName) ->
		@vars[varName] ? @parentScope?.getVarDef(varName)

	# Follow the chain of parent scopes up to find the enclosing module/entity def
	module: -> @parentScope.module()
	entity: -> @parentScope.entity()

@Module = class Module extends Scope
	constructor: (defs) ->
		@vars = {}
		@entityDefs = {}
		@imports = []
		@importedDefs = {}
		@mainEvent = null

		for def in defs
			if def instanceof UseDecl
				@imports.push def
			else if def instanceof EntityDef
				@entityDefs[def.name] = def
			else if def instanceof MainEventDef
				if @mainEventDef
					def.error "Multiple main events?"
				@mainEvent = def
			else if def instanceof EventDef
				def.error "Toplevel events not implemented"

	resolve: ->
		for i in @imports
			_.extend(@importedDefs, i.resolve(@).entityDefs)

		for k, def of @entityDefs
			def.resolve(this)
		@mainEvent?.resolve(this)

	getEntityDef: (ent) -> @entityDefs[ent] ? @importedDefs[ent]
	getVarDef: -> undefined

	module: -> this

@UseDecl = class UseDecl extends ASTNode
	constructor: (@name) ->

	resolve: (scope) ->
		@module = moduleLoader.load(@name)

class ArgScope extends Scope
	allowNew: false
	constructor: (@name, vars) ->
		@vars = {}
		@lets = []

		for v in vars
			@primaryVarName or= v.name if v instanceof ParamDef
			@vars[v.name] = v

	resolve: (parentScope) ->
		super
		for i in @lets
			i.resolve(this)
		return

@EntityDef = class EntityDef extends ArgScope
	allowNew: false #TODO: implement this
	constructor: (name, vars, body) ->
		super(name, vars)
		@body = body

	resolve: (parentScope) ->
		super
		@eventDefs = {}
		for def in @body
			def.resolve(this)
			if def instanceof EventDef
				@eventDefs[def.name] = def

	entity: -> this

@EventDef = class EventDef extends ArgScope
	constructor: (name, vars, body) ->
		super(name, vars)
		@body = new SeqBlock(body)

	resolve: (parentScope) ->
		super
		@body.resolve(this)

		# Events can't begin or end with a time expression
		# This is so composed events can be treated as atomic, and you can
		# put timers on either side of an eventTemplate
		if @body.beginsWithTimer()
			@error "Event definition can't begin with a time expression"
		if @body.endsWithTimer()
			@error "Event definition can't end with a time expression"

@MainEventDef = class MainEventDef extends ArgScope
	allowNew: true
	constructor: (vars, body) ->
		super('main', vars)
		@body = new SeqBlock(body)

	resolve: (scope) ->
		super
		@body.resolve(this)

#### Variable definitions

# Base class for all variables that live in a scope
@VarDef = class VarDef extends ASTNode
	constructor: (@name, @type, @init) ->
	resolve: (scope) ->
		@type.resolve(scope)
		@init.resolve(scope) if @init

	isEntity: -> @type instanceof EntityType

# Parameters to an entity or event
@ParamDef = class ParamDef extends VarDef

# LetDef is not itself the definition of a variable.
# Its left side may create variables
@LetDef = class LetDef extends ASTNode
	constructor: (@name, @r) ->
	resolve: (scope) ->
		@r.resolve(scope) if @r		

# A newDef creates an entity
@NewDef = class NewDef extends VarDef

#### Blocks

class Block extends Scope
	constructor: (@body) ->
		super

	resolve: (scope) ->
		super
		for def in @body then def.resolve(scope)

	beginsWithTimer: -> false
	endsWithTimer: -> false

@ParBlock = class ParBlock extends Block

@SeqBlock = class SeqBlock extends Block
	resolve: (parentScope) ->
		super

		# Verify time expressions
		tt = false
		for def in @body
			if tt and def.beginsWithTimer()
				def.error "Multiple adjacent time expressions"
			else
				tt = def.endsWithTimer()

	beginsWithTimer: -> @body.length and @body[0].beginsWithTimer()
	endsWithTimer: -> @body.length and @body[@body.length-1].endsWithTimer()

@RepeatBlock = class RepeatBlock extends Block
	constructor: (over, body, untilBody) ->
		super

		if over
			{@repeatVars, @countExpr, @counterVar} = over
		else
			@repeatVars = []
			@countExpr = @counterVar = null

		@body = new SeqBlock(body)
		@untilBody = if untilBody then new SeqBlock(untilBody) 

	resolve: (@parentScope) ->
		@countExpr.resolve(this, numberType) if @countExpr

		for {name, expr, width} in @repeatVars
			# Expressions are resolved in parent scope
			expr.resolve(@parentScope)

			# Declare this variable in the inner scope
			type = new BitsType(new Number(width))
			@vars[name] = new VarDef(name, type)

		if @counterVar
			# TODO: this should be number type (without width) once we
			# can convert between number and bits
			type = new BitsType(new Number(@counterVar.width))
			@vars[@counterVar.name] = new VarDef(@counterVar.name, type)

		@body.resolve(this)
		@untilBody?.resolve(this)

	getVarDef: Scope::getVarDef

	# Allow time expression before the repeat if it doesn't begin with a repeat
	beginsWithTimer: -> @body.beginsWithTimer()

	# Time expression at the end of a repeat is not used on exit
	endsWithTimer: -> false

@EventTemplate = class EventTemplate extends ASTNode
	constructor: (@entityVarName, @event, @args) ->

	resolve: (scope) ->
		# Make sure the referenced entity exists as a variable in the local scope
		# and that it is of entity type.

		if @entityVarName is 'this'
			@entityDef = scope.entity()
		else
			@entityVar = scope.getVarDef(@entityVarName)
			if not @entityVar
				@error "Entity var `#{@entityVarName}` not defined"
			if not @entityVar.isEntity()
				@error "Variable `#{@entityVarName}` not an entity"
			@entityDef = @entityVar.type.entityDef

		# Make sure the referenced event exists on the entity
		@eventDef = @entityDef.eventDefs[@event]
		if not @eventDef
			@error "Entity `#{@entityVarName}` of type `#{@entityVar.type.entityDef.name}` has no event `#{@event}`"

		if '0' of @args
			# If there is only one parameter, it is allowed to be passed without a name.
			# Move it the correct name here
			if not @eventDef.primaryVarName
				@error "Event arguments must be passed by name"
			@args[@eventDef.primaryVarName] = @args[0]
			delete @args[0]

		# Check to make sure the passed args exist
		for argName, argValue of @args
			if not argName of @eventDef.vars
				argValue.error "Event has no parameter `#{argName}`"
			argValue.resolve(scope)

	# Event definitions are not allowed to begin or end with time expressions.
	# This is verified on the event definition in EventDef::resolve
	beginsWithTimer: -> false
	endsWithTimer: -> false

@TimeTemplate = class TimeTemplate extends ASTNode
	constructor: (@expr) ->

	resolve: (scope) ->
		@expr.resolve(scope)

	beginsWithTimer: -> true
	endsWithTimer: -> true


#### Types

class TypeExpr extends ASTNode
@ValueType = class ValueType extends TypeExpr

@topType = topType = new (class TopType)()

typematch = (a, b) ->
	if a is topType then b
	else if b is topType then a
	else if a.constructor is b.constructor then a.match(b)
	else false

typematchReduce = (arr) -> _.reduce(arr, typematch, topType)

@typecheckTime = (timeExpr, loc) ->
	if not (timeExpr is topType or units.equal(timeExpr.unit, {units:{s:1}}))
		@loc.error "Time does not have unit [s]"

@Enum = class Enum extends ValueType
	constructor: (@values) ->
		for i in @values
			if not i instanceof SymbolValue
				@error "Enum member `#{@entityVarName}` not a symbol"

	resolve: (scope) ->
	check: (expr, context) ->
		typematch(this, expr.typecheck(context)) or @error("Passed value is not a subset")

	match: (other) ->
		#TODO: check that other is a subset of this
		other

@BitsType = class BitsType extends ValueType
	constructor: (@widthExpr) ->
	resolve: (scope) ->
		@widthExpr.resolve(scope)
	check: (expr, context) -> 
		parentType = expr.typecheck(context)
		if parentType isnt topType and not @widthExpr.evalUp(context, new NumberValue(parentType.width))
			@error "Width doesn't match"
		parentType

@BitsTypeInst = class BitsTypeInst
	constructor: (@width) ->
		console.assert @width, "Width can't be #{@width}"
	match: (other) -> @width == other.width and this

@NumberType = class NumberType extends ValueType
	constructor: (@unitRangeExpr) ->
	resolve: (scope) ->
		@unitRangeExpr.resolve(scope)

	check: (expr, context) -> 
		parentType = expr.typecheck(context)
		v = @unitRangeExpr.evalDown(context)
		unit = if v instanceof RangeExpr
			@unitRangeExpr.typecheck(context).unit
		else if v instanceof UnitLiteral
			v.unit
		else
			@error "Invalid type for number unit/range"

		typematch(new NumberTypeInst(unit), parentType) or
			@error "Passed number has incorrect units"

@NumberTypeInst = class NumberTypeInst
	constructor: (@unit, @min, @max) ->
		console.assert(@unit, 'unit is required')

	match: (other) ->
		# TODO: min, max
		other if units.equal(@unit, other.unit)


@EntityType = class EntityType extends TypeExpr
	constructor: (@entTypeName, @values) ->
	resolve: (scope) ->
		@entityDef = scope.module().getEntityDef(@entTypeName)
		@error "Entity type `#{@entTypeName}` not defined" unless @entityDef
		for k, v of @values
			v.resolve(scope)

	check: (expr, context) ->
		value = expr.evalDown(context)
		return false unless value.entityDef is @entityDef
		for ak, av of @values
			v = av.evalDown(context)
			if v instanceof NumberValue
				# hack: NumberValues don't carry type information that the
				# next entity will need, so we tie it in by converting to a
				# NumberLiteral.
				v = new NumberLiteral(v.num, av.typecheck(context).unit)
			value.set(ak, v)
		this

#### Expressions

class Expression extends ASTNode

@FlipExpr = class FlipExpr extends Expression
	constructor: (@l, @r) ->
	resolve: (scope) ->
		@l.resolve(scope)
		@r.resolve(scope)
	typecheck: (ctx) ->
		if not typematch((t = @l.typecheck(ctx)), @r.typecheck(ctx))
			@error "Types do not match" 
		return t

	evalDown: (context) -> @l.evalDown(context)
	evalUp: (context, v) -> @r.evalUp(context, v)


@RangeExpr = class RangeExpr extends Expression
	constructor: (@l, @r) ->
	resolve: (scope) ->
		@l.resolve(scope)
		@r.resolve(scope)
	typecheck: (ctx)->
		if not typematch((t = @l.typecheck(ctx)), @r.typecheck(ctx))
			@error "Types do not match" 
		if not t instanceof NumberType
			@error "Range type must be number"
		return t

	evalDown: (context) -> @
	evalUp: (context, v) ->
		return false unless v instanceof NumberValue
		vl = @l.evalDown(context)
		return false unless vl instanceof NumberValue
		vr = @r.evalDown(context)
		return false unless vr instanceof NumberValue
		return vl.num <= v.num < vr.num

OPS =
	'+': (a, b) -> a instanceof NumberValue and b instanceof NumberValue and new NumberValue(a.num+b.num)
	'-': (a, b) -> a instanceof NumberValue and b instanceof NumberValue and new NumberValue(a.num-b.num)
	'*': (a, b) -> a instanceof NumberValue and b instanceof NumberValue and new NumberValue(a.num*b.num)
	'/': (a, b) -> a instanceof NumberValue and b instanceof NumberValue and new NumberValue(a.num/b.num)
	'|': (a, b) -> a instanceof BitsValue and b instanceof BitsValue and a.or(b)
	'&': (a, b) -> a instanceof BitsValue and b instanceof BitsValue and a.and(b)
	'^': (a, b) -> a instanceof BitsValue and b instanceof BitsValue and a.xor(b)

@OpExpr = class OpExpr extends Expression
	constructor: (@e1, @op, @e2) ->
		@opfn = OPS[op]

	resolve: (scope) ->
		@e1.resolve(scope)
		@e2.resolve(scope)
	
	typecheck: (ctx) ->
		l = @e1.typecheck(ctx)
		r = @e2.typecheck(ctx)

		switch @op
			when '+', '-', '*', '/'
				if not (l instanceof NumberTypeInst and r instanceof NumberTypeInst)
					@error "Required number"
				new NumberTypeInst switch @op
					when '+', '-'
						@error "Units don't match" if not units.equal(l.unit, r.unit)
						l.unit
					when '*' then units.multiply(l.unit, r.unit, 1)
					when '/'
						units.multiply(l.unit, r.unit, -1)

			when '|', '&', '^'
				if not (l instanceof BitsType and r instanceof BitsType)
					@error "Required bits"
				if l.width != r.width
					@error "Widths must match"
				l

	evalDown: (context) -> 
		v1 = @e1.evalDown(context)
		if not isValue(v1) then return v1
		v2 = @e2.evalDown(context)
		if not isValue(v2) then return v2
		@opfn(v1, v2)
	evalUp: (context, v) ->
		@evalDown(context).equal(v)

@SliceExpr = class SliceExpr extends Expression
	constructor: (@e, @values) ->
	resolve: (scope) ->
		@e.resolve(scope)
		# TODO: check type
		for [l, r] in @values
			l.resolve(scope)
			r.resolve(scope)

	typecheck: (ctx) ->
		e = @e.typecheck(ctx)
		for [l, r] in @values
			@error "Left types don't match" if not typematch(l.typecheck(ctx), e)
		typematchReduce(r.typecheck(ctx) for [l, r] in @values) or @error "Right types don't match"

	evalDown: (context) ->
		ev = @e.evalDown(context)
		if ev instanceof AbstractValue then return abstract
		for [l, r] in @values
			if l.evalUp(context, ev) then return r.evalDown(context)
		return false

	evalUp: (context, v) ->
		for [l, r] in @values
			if r.evalUp(context, v)
				return @e.evalUp(context, l.evalDown(context))
		return false

@ConcatExpr = class ConcatExpr extends Expression
	constructor: (@vs) ->
	resolve: (scope) ->
		for {expr, width} in @vs
			expr.resolve(scope)

	typecheck: (ctx) ->
		totalWidth = 0
		for {expr, width}, i in @vs
			t = expr.typecheck(ctx)
			if t instanceof BitsTypeInst
				width ?= t.width
				if width != t.width
					@error "Width of component #{i} is incorrect"
			else if t is topType
				@error "Ignored width must be specified" if not width
			else
				@error "Concatenation argument must be bits"

			totalWidth += width
		new BitsTypeInst(totalWidth)

	evalDown: (context) ->
		totalWidth = 0
		b = for {expr, width} in @vs
			t = expr.typecheck(context)
			if t isnt topType then console.assert(t.width == width)
			totalWidth += width
			expr.evalDown(context)

		if _.every(b, isValue)
			BitsValue.join(b)
		else
			new AbstractValue(totalWidth)

	evalUp: (context, v) ->
		return true if not isValue v
		return false unless v instanceof BitsValue
		pos = 0
		success = true
		for {expr, width} in @vs
			t = expr.typecheck(context)
			if t isnt topType then console.assert(t.width == width)
			sv = v.slice(pos, pos+width)
			success and= expr.evalUp(context, sv)
			pos += width
		return success

@UnknownExpr = class UnknownExpr extends Expression
	constructor: ->
		@value = undefined
	resolve: ->
	typecheck: -> topType
	evalDown: -> ignore
	evalUp: (ctx, v) ->
		if @value
			@value.evalUp(ctx, v)
		else
			@value = v
			true

# Unknowns that are up-evaluated at compile time (e.g. bits(n))
@UnknownVarExpr = class UnknownVarExpr extends UnknownExpr
	evalDown: -> @value or ignore

@VarExpr = class VarExpr extends Expression
	constructor: (@varName) ->

	resolve: (scope) ->
		@varDef = scope.getVarDef(@varName)
		@error "Undefined variable `#{@varName}`" if not @varDef

	typecheck: (ctx) ->
		ctx.get(@varName).expr.typecheck(ctx)

	evalDown: (ctx) -> 
		{expr, context} = ctx.get(@varName)
		expr.evalDown(context)

	evalUp: (ctx, v) ->
		{expr, context} = ctx.get(@varName)
		expr.evalUp(context, v)

@DotExpr = class DotExpr extends Expression
	constructor: (@entityVar, @subentityName) ->
	resolve: (scope) ->
		@entityVar.resolve(scope)
		if not @entityVar.varDef.isEntity()
			@error "Variable `#{@entityVar.varName}` is not an entity"

	evalDown: (ctx) -> @entityVar.evalDown(ctx).getSubEntity(@subentityName)
	evalUp: -> false



#### Values / Literals

class Value
	resolve: (scope) ->
	evalDown: (context) -> @
	evalUp: (context, v) -> this.equal(v)

@UnitLiteral = class UnitLiteral extends Value
	constructor: (@unit) ->
	typecheck: -> new NumberTypeInst(@unit)

@BitsValue = class BitsValue extends BitArray
	resolve: (scope) ->
	typecheck: -> new BitsTypeInst(@length)
	evalDown: -> @
	evalUp: (context, v) -> @equal(v)
	toString: (x) ->
		if not x
			"'"+ super()
		else
			super(x)

@NumberValue = class NumberValue extends Value
	constructor: (@num) ->
		console.assert(@num?)
	typecheck: -> throw new Error "Can't typecheck a number value (not literal)"
	equal: (other) -> other instanceof NumberValue and other.num == @num
	toString: -> @num.toString()

@NumberLiteral = class NumberLiteral extends NumberValue
	constructor: (@num, @unit=units.dimensionless) ->
	typecheck: -> new NumberTypeInst(@unit)

@SymbolValue = class SymbolValue extends Value
	@internMap = {}
	@create = (str) ->
		unless str of @internMap
			@internMap[str] = new SymbolValue(str)
		@internMap[str]
	constructor: (@sym) ->
	typecheck: -> new Enum([this])
	equal: (other) -> other is this
	evalUp: (c, other) -> other is this
	toString: -> "$#{@sym}"

class IgnoreLiteral extends Value
	typecheck: -> topType
	evalUp: -> true
	toString: -> "ignore"

numberType = new NumberType(new UnitLiteral(units.dimensionless))

@ignore = ignore = new IgnoreLiteral

@AbstractValue = class AbstractValue extends IgnoreLiteral
	constructor: (@width) ->
	toString: -> "[loop-variant #{@width or ''}]"
	typecheck: -> if @width then new BitsTypeInst(@width)

@abstract = abstract = new AbstractValue

@isValue = isValue = (x) -> x and (x not instanceof IgnoreLiteral)

#### Error Handling

class LangError extends Error
	constructor: (@loc, msg) ->
		Error.call(this)
		@message = msg + " (at #{@loc})"
		Error.captureStackTrace(this, this.constructor)