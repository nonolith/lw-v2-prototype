
describe 'Symbol Literals',  ->
	s1 = sym('foo')
	s2 = sym('bar')
	s1a = sym('foo')

	it 'should parse', ->
		assert.strictEqual expr('$foo').sym, 'foo'

	describe 'equals', ->
		it 'should return true for equal', ->
			assert(s1.equal(s1a))
		it 'should return false for not equal', ->
			assert(!s1.equal(s2))

	describe 'evalDown', ->
		it 'should evaluate to self', ->
			assert.strictEqual(s2.evalDown(), s2)

	describe 'evalUp', ->
		it 'should succeed if equal', ->
			assert(!s2.evalUp(null, s1))
		it 'should fail if not equal', ->
			assert(s1.evalUp(null, s1a), 'up-compare 2')

describe 'Bit Literals', ->
	it 'should parse binary', ->
		assert.equal expr("'101").toString(), "'101"

	it 'should parse binary with b', ->
		assert.equal expr("'b101").toString(), "'101"

	it 'should parse hex with h', ->
		assert.equal expr("'hA5").toString(), "'10100101"

describe 'Unknowns', ->
	u = new n.UnknownExpr()

	it 'downEval should be ignore', ->
		assert.strictEqual u.evalDown(), n.ignore

	it 'set should succeed', ->
		assert(u.evalUp(null, sym('foo')))

	it 'should evalUp to set value', ->
		assert( u.evalUp(null, sym('foo')), 'eU delegates to set value 1')
		assert(!u.evalUp(null, sym('bar')), 'eU delegates to set value 1')

describe 'Slice Expressions', ->
	e = expr("v[$foo=$a, $bar=$b, $baz=$c]")
	cases = [['foo', 'a'], ['bar','b'], ['baz', 'c']]
	cases = ([sym(e1), sym(e2)] for [e1, e2] in cases)

	describe 'evalDown', ->
		it 'should return the matching expression', ->
			for [e1, e2] in cases
				c = ctx(v:e1)
				assert.strictEqual e.evalDown(c), e2

	describe 'evalUp', ->
		it 'should evalUp to the matching expression', ->
			for [e1, e2] in cases
				c = ctx({v:true})
				assert e.evalUp(c, e2)
				c.check('v', e1)

describe 'Flip Expressions', ->
	f = null

	it 'should parse', ->
		f = expr("left!right")

	it 'should evalDown to left expression', ->
		c = ctx(left:sym('leftSym'))
		check f.evalDown(c), sym('leftSym')

	it 'should evalUp to right expression', ->
		c = ctx(right:true)
		assert f.evalUp(c, sym('t'))
		c.check('right', sym('t'))

	it '<: x should be sugar for x!ignore', ->
		e = expr("<:x")
		assert e instanceof n.FlipExpr
		assert e.l instanceof n.VarExpr
		assert e.r is n.ignore

	it ':> x should be sugar for ignore!x', ->
		e = expr(":>x")
		assert e instanceof n.FlipExpr
		assert e.r instanceof n.VarExpr
		assert e.l is n.ignore

describe 'Range Expressions', ->
	r = expr("5..10")

	it 'should evalDown to self', ->
		assert.strictEqual(r.evalDown(), r, 'self-evaluates')

	it 'should fail if the value is below the range', ->
		assert !r.evalUp(null, num(2))

	it 'should succeed if the value is inside the range', ->
		assert r.evalUp(null, num(7))

	it 'should be bottom-inclusive', ->
		assert r.evalUp(null, num(5))
	
	it 'should be top-exclusive', ->	
		assert !r.evalUp(null, num(10))

	it 'should fail if the value is above the range', ->
		assert !r.evalUp(null, num(12))

	it 'should fail if the value is not a number', ->
		assert !r.evalUp(null, sym('foo'))

	it 'inherits the units of the right side', ->
		t = expr("5..10mm")
		assert.equal t.l.unit.symbol, 'mm'
		assert.equal t.r.unit.symbol, 'mm'

describe 'Concatenation Expressions', ->
	e = null
	defaults = {a: bitsv("1010"), b:bitsv("111"), c:bitsv("001")}
	correct = bitsv('1010111001')

	it 'should parse valid expressions', ->
		e = expr("[a:4, b:3, c:3]")

	it 'should evalDown to the concatenation of the bits', ->
		c = ctx(defaults)
		check e.evalDown(c), correct

	it 'should check the types of the expressions', ->
		expr("[$a:5, $b:1]") # TODO: should fail to resolve

	it 'should check the lengths of the expressions', ->
		expr("['1:1, '1111:1]") #TODO: should fail to resolve
		expr("['0:5, '1:1]")

	it 'should evalUp to the corresponding parts', ->
		c = ctx({a:true, b:true, c:true})
		assert e.evalUp(c, correct)
		c.check(defaults)

	it 'should handle ignore in the down direction, returning ignore', ->
		c = ctx({a:bitsv("1010"), b:n.ignore, c:n.ignore})
		check e.evalDown(c), n.ignore

	it 'should handle ignore in the up direction, not setting anything', ->
		c = ctx()
		assert e.evalUp(c, n.ignore)

describe 'Numbers', ->
	it 'can have units', ->
		expr("2mm")
		expr("3.3 V")

describe 'Operator expressions', ->
	it 'should parse and run', ->
		assert.equal expr("100 + 99").evalDown().num, 199
		assert.equal expr("5*8").evalDown().num, 40
		assert.equal expr("100/5").evalDown().num, 20
		assert.equal expr("20-7").evalDown().num, 13

	it 'should pass through ignore', ->
		assert.strictEqual expr("ignore+1").evalDown(), n.ignore

	it 'handles units', ->
		checkUnit expr("10ms + 12ms"), 's'
		checkUnit expr("30W * 1s"), 'J'
		checkUnit expr("1/1MHz"), 's'
		checkUnit expr("5V / 1kohm"), 'mA'

describe 'Parentheses', ->
	it 'should group expressions', ->
		assert.equal expr("(30+2)").evalDown().num, 32
		assert.equal expr("(30+2)/8").evalDown().num, 4
