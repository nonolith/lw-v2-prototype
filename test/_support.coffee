# Test support helpers
require('source-map-support').install()

global._      = require 'underscore'
global.assert = require 'assert'
global.n = require '../nodes'
global.units = require '../units'

exec   = require '../exec'
dump   = require '../file_wire'
moduleLoader = require '../module'

# Load Wire module by default
try
	globalTree = moduleLoader.load('Wire')
	globalTree.resolve()
catch error
	console.error "Error loading globals tree"
	console.error error.stack
	globalTree = null


# Functions to generate wrapped values
global.sym = _.bind(n.SymbolValue.create, n.SymbolValue)
global.bitsv = _.bind(n.BitsValue.fromString, n.BitsValue)
global.num = (nv) -> new n.NumberValue(nv)

global.mockScope = mockScope = {getVarDef: -> return true}

global.expr = (s) ->
	r = n.parse("<expr test>", s, 'valexpr')
	r.resolve(mockScope)
	return r

global.checkUnit = (e, unit) ->
	t = e.typecheck(null)
	assert t instanceof n.NumberTypeInst, "Checking unit, but it's not a Number"
	u = units.parse(unit)
	console.assert u, "Couldn't parse unit `#{unit}` -> #{units}"
	if not units.equal(t.unit, u)
		assert.equal t.unit, u


# A fake context that can be used to provide a specified value or check it
class MockContext
	constructor: (@d) ->
		for k, v of @d when v == true
			@d[k] = new n.UnknownExpr()

	get: (x) ->
		{context:@, expr:@d[x] or throw new Error("#{x} not in mock context")}

	check: (x, v) ->
		if v
			check(@d[x].value, v, x)
		else
			@check(k, v) for k, v of x

global.ctx = (d) -> new MockContext(d)

# Test equality of wrapped values
global.check = check = (actual, expected, v='') ->
	actual ?= n.ignore
	if not expected.evalUp(null, actual)
		throw new assert.AssertionError
			message: "Value for #{v} not as expected"
			expected: "#{v} = #{expected.toString()} (#{expected.constructor.name })"
			actual:   "#{v} = #{actual.toString()  } (#{actual.constructor.name   })"

global.runScript = ({vars, input, source, expected, resolveError, results}) ->
	(done) ->
		assert globalTree, "Can't do run tests without parsed globals"

		tree = n.parse('<test>', source)

		assert tree

		# Patch in global references
		for k, v of globalTree.entityDefs
			tree.importedDefs[k] = v

		# Get the main entity
		main = tree.mainEvent
		assert main

		# inject a `test` argument into the definition
		main.vars.test = new n.ParamDef('test', new n.EntityType('ValueDump', {}))

		try
			tree.resolve()
		catch e
			if resolveError
				if typeof resolveError is 'string' and e.message.indexOf(resolveError) < 0
					assert.equal e.message, resolveError
				return done()
			else
				throw e

		assert !resolveError, 'Resolve was supposed to throw an exception and did not'

		vars ?= {}
		vars['test'] = d = if input
			new dump.Read(dump.SimpleFormat, input)
		else
			new dump.Write(dump.SimpleFormat)

		if results
			for k, v of results
				vars[k] = new n.UnknownExpr(k)

		exec.runMain main, vars, (ctx, time) ->
			assert time != Infinity, "Completed at infinity"

			if expected and not input
				assert.equal d.get(), expected

			if results
				for k, v of results
					check(ctx.vars[k].value, v, k)

			done()
