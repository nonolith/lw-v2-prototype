describe 'main blocks', ->
	it 'should parse an empty block', runScript
		source: "main{}"
		expected: ""

	it 'should run a sequence of events', runScript
		source: """ main{
			w: Wire(initial = $l) = test.w
			#1s
			w.edge($h)
			#1s
			w.edge($l)
		}"""
		expected:"""
			0: w = $l
			1: w = $h
			2: w = $l
		"""

	it 'should capture data from events', runScript
		source: """ main{
			w: Wire(initial = $l) = test.w
			d: Wire(initial = $l) = test.d
			v: enum($h, $l) = ignore
			w.edge($h)
			d.level(v)
		}"""
		input: """
			0: d = $l
			0: w = $l
			1: d = $h
			2: d = $l
			3: d = $h
			4: w = $h
			5: d = $l
			"""
		results:
			v: sym('h')

describe 'scope', ->
	it 'should be an error if an undefined entity var is used', runScript
		source: """
		main{
			foo.bar()
		}
		"""
		resolveError: 'Entity var `foo` not defined'

	it 'should be an error if an undefined event is used', runScript
		source:"""
		main{
			test.foo()
		}
		"""
		resolveError: 'Entity `test` of type `ValueDump` has no event `foo`'

	it 'should be an error if an undefined variable is used', runScript
		source:"""
		main{
			new w: Wire() = test.wire
			w.level(foo)
		}
		"""
		resolveError: 'Undefined variable `foo`'

	it 'should be an error if an undefined type is used', runScript
		source:"""
		main{
			new w: Foo()
		}
		"""
		resolveError: 'Entity type `Foo` not defined'

	it 'should be an error to use a `new` decl in an event scope', runScript
		source: """
		entity Foo{
			event bar{
				new f: Foo()
		}}
		main{}
		"""
		resolveError: "`new` declaration not allowed here"

	describe "let declarations", ->
		it 'should create a variable binding and give it a value', runScript
			source: """
			main{
				w: Wire(initial = $l) = test.w
				let foo = $h
				# 1s
				w.edge(foo)
			}"""
			expected:"""
				0: w = $l
				1: w = $h
			"""
		it 'should work in event bodies too', runScript
			source: """
			entity E{
				w: Wire(initial=$l)
				event bar{
					let foo = $h
					w.edge(foo)
			}}
			main{
				new e: E(w=test.w)
				# 1s
				e.bar()
			}"""
			expected:"""
				0: w = $l
				1: w = $h
			"""
	
	describe 'Number type', ->
		it 'should parse', runScript
			source: """
				entity E{
					event x{
						a: number(ns)
						n: number(0..5V)
					}
				}
				main{
					new e: E()
					e.x(a=20ps, n=3.3V)
				}
			"""
			expected: ""


describe 'entity event calls', ->
	it 'should run the actions in the event', runScript
		source: """
			entity E{
				x: Wire(initial = $h)
				event foo{
					e2: enum($l, $h)
					x.edge($l)
					# 1s
					x.edge(e2)
				}
			}
			main{
				new t: E(x=test.w)
				# 1s
				t.foo($h)
			}
		"""
		expected: """
			0: w = $h
			1: w = $l
			2: w = $h
		"""

	it 'should upEvaluate the arguments', runScript
		source: """
		entity E{
			x: Wire(initial = $h)
			event foo{
				y: enum($l, $h)
				x.edge($l)
				x.level(y)
			}
		}
		main{
			v: enum($l, $h) = ignore
			new t: E(x=test.w)
			t.foo(v)
		}
		"""
		input:"""
			0: w = $h
			1: w = $l
		"""
		results:
			v: sym('l')


	it 'should access the enclosing entity with `this`', runScript
		source: """
			entity E{
				x: Wire(initial = $h)
				event foo{
					x.edge($l)
					# 1s
					x.edge($h)
				}
				event bar{
					this.foo()
				}
			}
			main{
				new t: E(x=test.w)
				# 1s
				t.bar()
			}
		"""
		expected: """
			0: w = $h
			1: w = $l
			2: w = $h
		"""

	it 'should evaluate the argument of bits(n) types', runScript
		source: """
			entity E{
				x: Wire(initial = $h)
				event transfer{
					d: bits(6)
				}
			}
			main{
				new t: E(x=test.w)
				t.transfer('101000)
			}
		"""


describe 'timer expressions', ->
	it 'should specify the time before the event', runScript
		source: """
			main{
				w:Wire(initial=$l) = test.w
				# 2s
				w.edge($h)
				# 3s
				w.edge($l)
			} """
		expected: """
			0: w = $l
			2: w = $h
			5: w = $l
		"""

	it 'should fail if multiple are used on the same primitive event', runScript
		source: """
			main{
				w:Wire() = test.wire
				# 5s
				# 6s
				w.edge($h)
			}"""
		resolveError: "Multiple adjacent time expression"

	it 'should push the time to the first primitive event', runScript
		source: """
			entity E{
				w: Wire(initial=$l)
				event foo{
					w.edge($h)
					# 3s
					w.edge($l)
				}
			}
			main{
				new e: E(w=test.w)
				# 2s // should also run in the correct context
				e.foo()
			}
		"""
		expected: """
			0: w = $l
			2: w = $h
			5: w = $l
		"""

	it 'should not allow event defs to begin with a time expression', runScript
		source: """
			entity E{
				w: Wire()
				event foo{
					# 5s
					w.edge($h)
				}
			}
			main {}
		"""
		resolveError: "Event definition can't begin with a time expression"


	it 'should not allow event defs to end with a time expression', runScript
		source: """
			entity E{
				w: Wire()
				event foo{
					w.edge($h)
					# 5s
				}
			}
			main {}
		"""
		resolveError: "Event definition can't end with a time expression"

	it 'should upEval the time before matching an event', runScript
		source: """
			main{
				w:Wire(initial=$l) = test.w
				t1: number(s)
				t2: number(s)
				# t1
				w.edge($h)
				# t2
				w.edge($l)
			} """
		input: """
			0: w = $l
			2: w = $h
			5: w = $l
		"""
		results: {t1:new n.NumberValue(2), t2:new n.NumberValue(3)}

	it 'should upEval the time before the first primitive of a definition, in lexical context', runScript
		source: """
			entity E{
				w: Wire(initial=$l)
				event foo{
					w.edge($h)
				}
			}
			main{
				new e: E(w=test.w)
				t1: number(s)
				# t1
				e.foo()
			}
		"""
		input: """
			0: w = $l
			7: w = $h
		"""
		results: {t1: new n.NumberValue(7)}

	it 'can access the dynamic loop context', runScript
		source: """
			main{
				w:Wire(initial=$l) = test.w
				repeat 4 (d:1 = '1101){
					# 1s
					w.edge($h)
					# d['0=1s, '1=2s]
					w.edge($l)
				}
			}
		"""
		expected: """
			0: w = $l
			1: w = $h
			3: w = $l
			4: w = $h
			6: w = $l
			7: w = $h
			8: w = $l
			9: w = $h
			11: w = $l
		"""

	it 'treats the ignore value as 0', runScript
		source: """
			main{
				w: Wire(initial=$l) = test.w
				w.edge($h)
				#ignore
				w.edge($l)
			}
		"""
		expected: """
			0: w = $l
			0: w = $h
			0: w = $l
		"""

describe 'repeat blocks', ->
	it 'should run the specified number of iterations', runScript
		source: """
			main{
				w: Wire(initial = $l) = test.w
				repeat 3 () {
					# 1s
					w.edge($h)
					# 1s
					w.edge($l)
				}until{
					# 1s
					w.edge($h)
				}
			}
		"""
		expected: """
			0: w = $l
			1: w = $h
			2: w = $l
			3: w = $h
			4: w = $l
			5: w = $h
			6: w = $l
			7: w = $h
		"""

	it 'should run the actions for each part of the input', runScript
		source: """
			main{
				w: Wire(initial = $l) = test.w
				repeat 3 (bit:1='101){
					# 1s
					w.edge(bit['1=$h, '0=$l])
				}
			}
		"""
		expected: """
			0: w = $l
			1: w = $h
			2: w = $l
			3: w = $h
		"""

	it 'should capture data from the signal and stop on the until event', runScript
		source: """main{
				c: Wire(initial = $l) = test.c
				d: Wire(initial = $l) = test.d
				s: Wire(initial = $l) = test.s
				n: number() = ignore
				data: bits(4) = ignore
				repeat n (bit:1 = data){
					c.edge($h)
					c.edge($l)
					d.level(bit['1=$h,'0=$l])
				}until{
					s.edge($h)
				}
				c.edge($h) // Test to make sure the event following the loop works
			}
		"""
		input:"""
			0: c = $l
			0: d = $l
			0: s = $l

			1: c = $h
			1: d = $h
			2: c = $l

			3: c = $h
			3: d = $h
			4: c = $l

			5: c = $h
			5: d = $l
			6: c = $l

			7: c = $h
			7: d = $h
			8: c = $l

			9: s = $h
			10: c = $h
		"""
		results: {data: bitsv('1101'), n:new n.NumberValue(4)}

	it 'should have a counter variable', runScript
		source: """
			main{
				w: Wire(initial = $h) = test.w
				repeat 3 (i:1) {
					# 1s
					w.level(i['1=$h,'0=$l])
				}
			}
		"""
		expected: """
			0: w = $h
			1: w = $l
			2: w = $h
			3: w = $l
		"""

	it 'uses the time before the loop on the first iteration, then the time at the end', runScript
		source: """
			main{
				w: Wire(initial=$l) = test.w
				# 5s
				repeat 2 (){
					w.edge($h)
					#1s
					w.edge($l)
					#2s
				}
				# 3s
				w.edge($h)
			}
		"""
		expected: """
			0: w = $l
			5: w = $h
			6: w = $l
			8: w = $h
			9: w = $l
			12: w = $h
		"""

	it 'should inherit the enclosing scope', runScript
		source: """
			entity E{
				w: Wire(initial = $l)
				event f{
					x: enum($l, $h)
					repeat 1 (){
						w.level(x)
					}
				}
			}
			main{
				new e: E(w=test.w)
				let y = $h
				#1s
				e.f(y)
			}
		"""
		expected: """
			0: w = $l
			1: w = $h
		"""