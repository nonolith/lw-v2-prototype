{BitArray} = require('../bitarray')

fs = _.bind(BitArray.fromString, BitArray)

describe 'BitArray',  ->
	it 'should round-trip in binary', ->
		for s in ["1", "10101110101", "10111110", "10100100010101010001001001001"]
			assert.equal(BitArray.fromString(s).toString(), s)

	it 'should round-trip in hex', ->
		for s in ["f", "1234567", "5555", "abc"]
			assert.equal(BitArray.fromString(s, 16).toString(16), s)

describe 'BitArray.fromInt', ->
	it 'should turn an integer into a bitarray (MSB first)', ->
		d = BitArray.fromInt(0xA57F, 16)
		assert.equal d.toString(16), 'a57f'

	it 'should work when not aligned', ->
		d = BitArray.fromInt(5, 5)
		assert.equal d.toString(), '00101'

	it 'should cut off the excess bits', ->
		d = BitArray.fromInt(5, 1)
		assert d.equal(BitArray.fromInt(1, 1))

describe 'BitArray.slice', ->
	it 'should behave like string.slice on all inputs', ->
		check = (s) ->
			f = fs(s)
			for i in [0...s.length]
				for j in [i+1..s.length]
					#a = f.slice(i, j).toString()
					#b = s.slice(i, j)
					#console.log i, j, j-i, a==b, a, b
					assert.equal(f.slice(i, j).toString(), s.slice(i, j))

		check '10100101'
		check '1010111011'
		check '10101110110101010111'
		check '1001'
		check '1011111010101110'
		check '10111110101011101'

describe 'BitArray.join', ->
	it 'should work', ->
		b = BitArray.fromByte(0x57)
		c = BitArray.fromByte(0x0f)

		a = BitArray.join([c, b])
		assert.equal(a.length, 16)
		assert.equal(a.toHexString(), '0f57')

		assert.equal(a.slice(4, 8).toHexString(), 'f')
		assert.equal(a.slice(8, 12).toHexString(), '5')

		d = BitArray.join([a, c.slice(6,8), b.slice(1, 4), a.slice(0, 8)])
		assert.equal(d.length, 16 + 2 + 3 + 8)
		assert.equal(d.toString(), '00001111 01010111 11 101 00001111'.replace(/\s/g, ''))

		d = BitArray.join([c.slice(6,8), b.slice(1, 4), b.slice(0, 4)])
		assert.equal(d.toString(), '11 101 0101'.replace(/\s/g, ''))

		d = BitArray.join([fs('abc', 16), fs('f', 16), fs('1234', 16)])
		assert.equal(d.toString(16), 'abcf1234')

		d = BitArray.join([fs('ab', 16), fs('f', 16), fs('12345', 16), fs('098', 16)])
		assert.equal(d.toString(16), 'abf12345098')

	it 'should work when the result is < 8 bits', ->
		d = BitArray.join([fs('1'), fs('1'), fs('0'), fs('1')])
		assert.equal(d.toString(2), '1101')

	it 'should work when the result is > 8 but not %8 bits', ->
		t = "1010111010001"
		d = BitArray.join(fs x for x in t)
		assert.equal(d.toString(2), t)

	it 'should work when the result is > 8 and %8 bits', ->
		t = "1010111010001001"
		d = BitArray.join(fs x for x in t)
		assert.equal(d.toString(2), t)

describe 'BitArray bitwise ops', ->
	it 'and should return the correct result', ->
		assert.equal(fs("101").and(fs("011")).toString(), "001")
	it 'xor should return the correct result', ->
		assert.equal(fs("FF", 16).xor(fs("A5", 16)).toString(16), "5a")
	it 'or should return the correct result', ->
		assert.equal(fs("fa10", 16).or(fs("015f", 16)).toString(16), "fb5f")

	it 'equal should return true when inputs are equal', ->
		assert( fs('101011110101').equal(fs('101011110101')), 'eq +')

	it 'equal should return false when inputs are not equal', ->
		assert(!fs('101011110101').equal(fs('101011110100')), 'eq -')