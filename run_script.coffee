#!/usr/bin/env coffee

require('source-map-support').install()

fs        = require 'fs'
{inspect} = require 'util'
lang      = require './lang'
exec      = require './exec'
n         = require './nodes'
dump      = require './file_wire'
moduleLoader = require './module'
bci       = require './xmos-bci/xmos-bci'

tree = moduleLoader.loadFile(process.argv[2])

# Get the main entity
main = tree.mainEvent
console.assert(main)

varNames = (v.name for k, v of main.vars when not v.isInternal)
#console.log "Main has vars: #{varNames.join(', ')}"

vars = {}

# Actions to run when parse is complete
after = []

parseArg = (name, argStr, type) ->
	if type instanceof n.ValueType and argStr == '?'
		# BitsType, NumberType, EnumType, etc... can all be printed identically
		r = new n.UnknownExpr()
		after.push ->
			radix = if r.value instanceof n.BitsValue and r.value.length>8 then 16 else 2
			console.log "#{name} = #{r.value?.toString(radix) or 'NOT SET'}"
		r
	else if type instanceof n.BitsType
		if argStr.slice(0, 2) == 'r:'
			n.BitsValue.fromBuffer(fs.readFileSync(argStr.slice(2)))
		else
			n.BitsValue.fromString(argStr)

	else if type instanceof n.EntityType
		if type.entityDef is dump.def
			m = argStr.slice(0, 2)
			fname = argStr.slice(2)

			switch m
				when 'x:'
					new bci.Device(fname)
				when 'w:'
					dw = new dump.Write(dump.VCDFormat)
					after.push ->
						fs.writeFileSync(fname, dw.get())
					dw
				when 'r:'
					data = fs.readFileSync(fname, 'utf8')
					new dump.Read(dump.VCDFormat, data)
				else
					throw new Error("Invalid r/w file mode: #{m}")


for arg in process.argv.slice(3)
	[k, v] = arg.split('=')
	argDef = main.vars[k]

	if argDef not instanceof n.ParamDef
		console.warn "Unknown cmdline argument `#{k}`"
		continue

	val = parseArg(k, v, argDef.type)
	if val
		vars[k] = val
	else
		console.warn "Invalid value or type for `#{k}`"

exec.runMain main, vars, (ctx) ->
	i() for i in after
	console.log "Done"