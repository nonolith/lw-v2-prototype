{
var n = require("./nodes")
var units = require("./units")

function p(node){
	node.startPos = offset()
	node.text = text()
	node.input = input
	node.file = options.fname
	return node;
}

function err(str){
	throw new Error(""+str+" (at "+options.fname+":"+line()+")")
}
}

start
  = __ ts:toplevel* !. {return p(new n.Module(ts))}

toplevel
  = entitydef
  / maineventdef
  / eventdef
  / include

entitydef = "entity" _ name:identifier __ "{" __ vars:vardef* body:entitybody* "}" __
	{return p(new n.EntityDef(name, vars, body))}

entitybody
  = eventdef

include = "use" _ name:identifier __
	{return p(new n.UseDecl(name))}

// Variable definitions
vardef
	= letdef
	/ newdef
	/ paramdef

varinit = (_ "=" _ e:valexpr) {return e}

paramdef = name:identifier _ ":" _ type:typeexpr init:varinit? BREAK
	{return p(new n.ParamDef(name, type, init||null))}

letdef = "let" _ name:identifier _ r:("=" _ r:valexpr {return r})? BREAK
	{return p(new n.LetDef(name, r))}

newdef = "new" _ name:identifier _ ":" _ type:typeexpr init:varinit? BREAK
	{return p(new n.NewDef(name, type, init||null))}

// Events
eventdef = "event" _ name:identifier __ "{" __ vars:vardef* body:eventbody* "}" __
	{return p(new n.EventDef(name, vars, body))}

maineventdef = "main" _ "{" __ vars:vardef* body:eventbody* "}" __
	{return p(new n.MainEventDef(vars, body))}

eventbody
	= parblock
	/ seqblock
	/ repeatblock
	/ subevent
	/ time


parblock = "par" __ "{" __  body:eventbody* "}" __
	{return p(new n.ParBlock(body))}

seqblock = "seq" __ "{" __  body:eventbody* "}" __
	{return p(new n.SeqBlock(body))}

repeatblock = "repeat" _ over:repeatover? __
	"{" __  body:eventbody* "}" __ 
	until:repeatuntil?
	{return p(new n.RepeatBlock(over, body, until))}

	repeatover = count:valexpr _ "("
		countervar:repeatcounter?
		vars:((v:repeatvar commasep){return v})*
	")" {return {countExpr:count, repeatVars:vars, counterVar:countervar}}

	repeatcounter = name:identifier _ ":" _ width:integer commasep
		{return {name:name, width:width}}

	repeatvar = name:identifier _ ":" _ width:integer _ "=" _ expr:valexpr
		{return {name:name, width:width, expr:expr}}

	repeatuntil = "until" __ "{" __ body:eventbody* "}" __
		{return body}

subevent = entity:identifier _ "." _ event:identifier _ "(" args:call_args ")" BREAK
	{return p(new n.EventTemplate(entity, event, args))}

time = "#" _ v:valexpr __
	{return p(new n.TimeTemplate(v))}


typeexpr
	= enum
	/ bitstype
	/ numbertype
	/ entityinst

enum = "enum" _ "(" vs:symbol_list ")" _
	{return p(new n.Enum(vs))}

symbol_list = __ vs:((i:symbolliteral commasep){return i})* {return vs}

bitstype = "bits" _ "(" _ val:valexpr ")" _
	{return p(new n.BitsType(val))}

numbertype = "number" _ "(" range:(rangeexpr/unitliteral) ")" _
	{return p(new n.NumberType(range))}

entityinst = tp:identifier _ "(" vs:call_args ")" _
	{return p(new n.EntityType(tp, vs))}

valexpr = valexpr_flip

valexpr_flip
	= l:valexpr_range "!" _ r:valexpr_range {return p(new n.FlipExpr(l, r))}
	/ "<:" _ x:valexpr_range {return p(new n.FlipExpr(x, n.ignore))}
	/ ":>" _ x:valexpr_range {return p(new n.FlipExpr(n.ignore, x))}
	/ valexpr_range

valexpr_range
	= rangeexpr
	/ valexpr_sum

	rangeexpr = l:valexpr_sum ".." _ r:valexpr_sum {
		if (l instanceof n.NumberLiteral &&
			l.unit === units.dimensionless
			&& r instanceof n.NumberLiteral){
			// e.g. 20..30ns -> 20ns..30ns
			l = new n.NumberLiteral(l.num, r.unit)
		}
		return p(new n.RangeExpr(l, r))
	}

valexpr_sum
	= l:valexpr_product op:[-+^|] _ r:valexpr_sum {return p(new n.OpExpr(l, op, r))}
	/ valexpr_product

valexpr_product
	= l:valexpr_slice op:[*/&] _ r:valexpr_product {return p(new n.OpExpr(l, op, r))}
	/ valexpr_slice

valexpr_slice
	= e:valexpr_atom "[" vs:slice_list "]" _ {return p(new n.SliceExpr(e, vs))}
	/ valexpr_atom

slice_list = __ vs:((k:valexpr "=" _ v:valexpr commasep){return [k,v]})*
	{return vs}

valexpr_atom
	= symbolliteral
	/ bitsliteral
	/ ignoreliteral
	/ dotexpr
	/ variable
	/ numberliteral
	/ concatexpr
	/ ("(" _ v:valexpr ")" _ {return v})

ignoreliteral = "ignore" _
	{return n.ignore}

concatexpr = "[" vs:((v:widthfield commasep){return v})+ "]" _
	{return p(new n.ConcatExpr(vs))}

widthfield = e:valexpr ":" _ w:integer _ {return {expr:e, width:w}}

symbolliteral "symbol literal" = "$" i:identifier _
	{return n.SymbolValue.create(i)}

bitsliteral "bit literal"
	= "'h" v:$([0-9a-fA-F]+) _ {return n.BitsValue.fromString(v, 16)}
	/ "'" "b"? v:$([01]+) _    {return n.BitsValue.fromString(v, 2)}

numberliteral = v:floatnumber _ u:unit _
	{return new n.NumberLiteral(v*u.scale, u)}

unitliteral = u:unit
	{return new n.UnitLiteral(u)}

unit = u:$([a-zA-Z]+){return units.parse(u) || err("Unknown unit "+u)}
     / "" {return units.dimensionless}

// only for sub-entities of entities
dotexpr = v:variable "." _ s:identifier _
	{return p(new n.DotExpr(v, s))}

variable = name:identifier _
	{return p(new n.VarExpr(name))}

identifier_list = __ vs:((i:identifier _ commasep){return i})* {return vs}
assign_list = __ vs:((k:identifier _ "=" _ v:valexpr commasep){return [k,v]})*
	{var r={}; for (var i=0; i<vs.length;i++) r[vs[i][0]]=vs[i][1]; return r}
call_args
	= (_ v:valexpr !(__ "=")) {return {0:v}}
	/ (__ l:assign_list) {return l}

commasep = ( "," / TERMINATOR / &(__ [)\]]) ) __

WHITESPACE
  = [\u0009\u000B\u000C\u0020\u00A0\uFEFF\u1680\u180E\u2000-\u200A\u202F\u205F\u3000]
  / "\\" TERM { return ''; }

TERM
  = "\r"? "\n" { return '\n'; }

TERMINATOR
  = (comment? TERM)+

BREAK = _ TERMINATOR __

comment = "//" (!TERM .)*

_ "space" = WHITESPACE* {return ''}
__ "space" = (WHITESPACE / TERMINATOR)* {return ''}

identifier "identifier" = !(reserved ![0-9a-zA-Z_]) n:$([a-zA-Z_][0-9a-zA-Z_]*)
	{return n}

integer "number" = vs:$([0-9]+)
	{return parseInt(vs, 10)}

floatnumber "number" = vs:$("-"?[0-9]+("."[0-9]+)?)
	{return parseFloat(vs, 10)}

reserved
  = "entity"
  / "event"
  / "enum"
  / "par"
  / "seq"
  / "bits"
  / "ignore"
  / "let"
  / "new"
