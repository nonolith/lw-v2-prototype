# Contexts and other lexically-associated state.

_ = require 'underscore'
{isValue} = nodes = require './nodes'

# A **Context** is a compile-time-bound namespace for variables. A context is
# created for each instance of each Scope in the AST -- one for each call
# site. 
@Context = class Context
	# Contexts form two hierarchies via parent pointers:
	#
	#  * `stackParent` is the dynamically-enclosing context, e.g. the caller
	#  * `lexicalParent` is the lexically-enclosing context, e.g. the entity
	#
	# Inner blocks such as `repeat` have `@stackParent` == `@lexicalParent` because
	# the caller is also the lexical surroundings.
	lexicalIsStack: false
	constructor: (@stackParent, @lexicalParent, @vars = {}) ->
		if @lexicalIsStack
			console.assert @stackParent is @lexicalParent

	initVars: (varDefs) ->
		# Create expressions for the variable nodes in this scope
		for varName, varDef of varDefs
			@vars[varName] = varDef.expressionInScope(@)

		for varName, varDef of varDefs when varDef not instanceof nodes.LetDef
			type = varDef.type.check(@vars[varName], this)
			if not type
				varDef.error "err: check #{varDef.type?.constructor.name} returned null"		
	# Get a variable expression and the context in which it's defined.
	get: (v) ->
		if @vars[v]
			{context:@, expr:@vars[v]}
		else
			#If it's not here, delegate to the upper lexical scope.
			console.assert @lexicalParent
			@lexicalParent.get(v)

	# Enter a context -- create a runtime context for this static context, and
	# attach it to `exec`.
	enter: (exec) ->
		lx = if @lexicalIsStack then exec.context else @lexicalParent
		exec.context = new @runtimeContext(exec.context, lx, @)
		exec.context.onEnter()
		exec.context

	# Leave a context -- pop the runtime context from the `exec`
	leave: (exec) ->
		console.assert exec.context.staticContext is this, "mismatched runtime context"
		exec.context.onLeave()
		exec.context = exec.context.stackParent

	# Get the set of device instances accessible through this namespace. This
	# is for the main event, would need to look at the lexicalParent to be
	# useful in entity events.
	getDeviceSet: -> namespaceDeviceSet(@vars)


# These expressionInScope methods convert a VarDef AST node from a Scope to an
# expression to put in a Context namespace.
nodes.ParamDef::expressionInScope = (context) ->
	# Double-dispatch back to the context, as different context types handle
	# args differently. Or, if parameter not passed, use the default (without
	# binding, so in this context)
	context.argToExpr(@name) or
		@init or
		@error "No value or default for `#{@name}`"

nodes.LetDef::expressionInScope = -> @r or new nodes.UnknownVarExpr()
nodes.NewDef::expressionInScope = -> new EntityInst(@type.entityDef)

# **Runtime Contexts** store the runtime-mutable state associated with a
# scope. As the execution enters a region of the FA associated with a new
# scope, a new `RuntimeContext` is created. A static context's
# `@runtimeContext` property selects `RuntimeContext` subclass that will be
# created.
Context::runtimeContext = class RuntimeContext
	constructor: (@stackParent, @lexicalParent, @staticContext) ->
		@vars = {}

	get: (v) ->
		if (expr = @vars[v] or @staticContext.vars[v])
			{context: @, expr}
		else
			@lexicalParent.get(v)

	# `onEnter` and `onLeave` can be overridden by subclasses to take action
	# when execution enters and leaves the region associated with this
	# context.
	onEnter: ->
	onLeave: ->

@MainContext = class MainContext extends Context
	constructor: (module, @mainDef, @args) ->
		super()
		@initVars(@mainDef.vars)

	argToExpr: (argName) -> @args[argName]

# Context subclass for an entity event call
@CallContext = class CallContext extends Context
	constructor: (@stackParent, @lexicalParent, @eventCall,  @eventDef) ->
		super(@stackParent, @lexicalParent)
		@initVars(@eventDef.vars)

	argToExpr: (name) ->
		new BoundExpression(@eventCall.args[name], @stackParent) if @eventCall.args[name]

	# A wrapper that binds an expression to a particular context. Used in the
	# implementation of parameters.
	class BoundExpression
		constructor: (@expression, @context) ->
		typecheck: -> @expression.typecheck(@context)
		evalDown: (ctx) ->
			console.assert ctx.stackParent is @context or ctx.stackParent.staticContext is @context
			@expression.evalDown(ctx.stackParent)
		evalUp: (ctx, v) ->
			console.assert ctx.stackParent is @context or ctx.stackParent.staticContext is @context
			@expression.evalUp(ctx.stackParent, v)

@PrimitiveCallContext = class PrimitiveCallContext extends CallContext
	# Primitives don't bind their arguments, because they don't create their
	# own runtime context -- enter and leave are not called on primitive
	# contexts.
	#argToExpr: (name) -> @eventCall.args[name]

# Context for the body of a repeat block.
@RepeatContext = class RepeatContext extends Context
	lexicalIsStack: true
	constructor: (parent, @repeatDef) ->
		super(parent, parent)

		# In the static context, repeat variables are unknown
		for v in @repeatDef.repeatVars
			@vars[v.name] = new nodes.AbstractValue(v.width)

		if (cv = @repeatDef.counterVar)
			@vars[cv.name] = new nodes.AbstractValue(cv.width)

		return

	runtimeContext: class RepeatRuntimeContext extends RuntimeContext
		onEnter: ->
			@repeatDef = @staticContext.repeatDef
			@count = @repeatDef.countExpr.evalDown(@stackParent)
			@i = 0

			# Set up repeat vars
			@repeatVars = for v in @repeatDef.repeatVars
				@vars[v.name] = new RepeatVarExpression(@, v) 

			# and loop counter
			if (cv = @repeatDef.counterVar)
				@vars[cv.name] = new RepeatCountExpression(@, cv.width)

			return

		onLeave: ->
			i.done(this) for i in @repeatVars
			if @repeatDef.countExpr
				n = new nodes.NumberValue(@i+1)
				@repeatDef.countExpr.evalUp(@stackParent, n)

	# An expression for the split variables in a repeat block.
	# It downEvaluates to a particular part of the field, and the 
	# up-evaluation is joined at the end of the loop.
	class RepeatVarExpression
		constructor: (ctx, {@name, @width, @expr}) ->
			@downVal = @expr.evalDown(ctx.stackParent)
			if isValue(@downVal)
				if @downVal.length != ctx.count*@width
					ctx.repeatDef.error "Width of #{@name} (#{@downVal.length}) inconsistent with loop length (#{ctx.count}*#{@width})"
			@upVals = []

		typecheck: -> new nodes.BitsTypeInst(@width)

		# Check and up-evaluate the mutable state at the end of the loop
		done: (ctx) ->
			u = @upVals.slice(0, ctx.i+1)
			if _.filter(u, _.identity).length is ctx.i+1
				v = nodes.BitsValue.join(u)
				@expr.evalUp(ctx.stackParent, v)

		evalDown: (ctx) ->
			if isValue(@downVal)
				@downVal.slice(ctx.i*@width, (ctx.i+1)*@width)
			else
				nodes.ignore
		
		evalUp: (ctx, v) ->
			if not @upVals[ctx.i]
				@upVals[ctx.i] = v
				true
			else
				# If it's already been set, verify it matches the existing value
				@upVals[ctx.i].evalUp(ctx, v)

	# Expression for the counter of a repeat block
	class RepeatCountExpression
		constructor: (ctx, @width) ->
		typecheck: -> new nodes.BitsTypeInst(@width)
		evalDown: (ctx) -> nodes.BitsValue.fromInt(ctx.i, @width)
		evalUp: (ctx, v) -> @evalDown(ctx).evalUp(ctx, v)

	class @TimerExpr
		constructor: (@before=nodes.ignore, @atEnd=nodes.ignore) ->
		typecheck: (ctx) ->
			@nodes.typecheckTime(@before, ctx.repeatDef)
			@nodes.typecheckTime(@atEnd, ctx.repeatDef)
			new nodes.NumberTypeInst({units:{s:1}})
		delegate: (ctx) ->
			if not ctx.i? then nodes.abstract
			else if ctx.i == 0 then @before
			else @atEnd
		evalDown: (ctx) ->	@delegate(ctx).evalDown(ctx)
		evalUp: (ctx, v) -> @delegate(ctx).evalUp(ctx, v)


# Gets the set of primitive devices / resources accessible through a namespace
namespaceDeviceSet = (namesDict) ->
	_.union (v.getDeviceSet() for k, v of namesDict when v instanceof EntityInst)...

# **EntityInst** is the abstract base for the runtime, device-specific
# instances of entities. It stores the parameter values associated with the
# instance.

# It's duck-type compatible with Context, as part of an entity's
# lexical scope chain, and with Expression, as a value that is passed as a
# parameter to other entities.
@EntityInst = class EntityInst
	constructor: (@entityDef) ->
		@values = {}

	# It behaves as a value for use in variable defaults (for the main block)
	evalDown: -> @

	# Sets a setting on the entity
	set: (k, v) ->
		def = @entityDef.vars[k] #not getVarDef because you can only assign local
		if not def
			throw new Error("Value assigned to unknown var `#{k}` on `#{@entityDef.name}`")
		else if k of @values
			# If the setting already has a value, error if the new value is not
			# equivalent to it.
			if def.isEntity() or not @values[k].evalUp(null, v)
				throw new Error("Can't reassign entity var `#{k}` on `#{@entityDef.name}`")
		else
			if not def.type.check(v, this)
				throw new Error("Value assigned to `#{k}` on `#{@entityDef.name}` is of incorrect type")
			@values[k] = v

	get: (k) -> 
		if k is 'this'
			{context:@, expr:@}
		else
			console.assert @values[k], "Entity-level lookup of #{k} failed"
			{context:@, expr:@values[k]}

	getDeviceSet: -> namespaceDeviceSet(@values)

	# To be overridden by subclasses: 

	# Override to allow getting a sub-entity -- `cee.chanA`.
	getSubEntity: (name) -> false

	# Called before the program starts
	prepare: ->

	# Called after the program completes
	after: ->

	callContext: CallContext

# An **EntityPrimitive** is a runtime instance of an entity whose actions are
# implemented in CoffeeScript. This is an abstract base class for
# implementations to derive.
@EntityPrimitive = class EntityPrimitive extends EntityInst
	getDeviceSet: -> [this]
	callContext: PrimitiveCallContext
