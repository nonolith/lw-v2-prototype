
radix_width = (o) -> {2:1, 16:4}[o]
pb = (b) -> (0x100|b).toString(2).slice(-8)

exports.BitArray = class BitArray
	constructor: (@array, @length) ->

	getBit: (b) -> 1 & (@array[Math.floor(b/8)] >> (7-b%8))

	slice: (a, b) ->
		#console.log 'slice', @toString(), a, b

		outBits = b-a
		outBytes = Math.ceil(outBits/8)

		#if a%8 == 0 and b%8 == 0
		#	return @sliceBytes(a/8, b/8)

		# shift to translate b bit off the end of the byte
		s = (@length - b)%8
		sb = @array.length - Math.floor((@length - b)/8) - outBytes

		outArr = new Uint8Array(outBytes)

		for i in [0...outBytes]
			outArr[i] = @array[i+sb-1]<<(8-s) | @array[i+sb]>>s
			#console.log i, sb, s, pb(@array[i+sb-1]), pb(@array[i+sb]), pb(outArr[i])

		outArr[0] &= (0xff>>((8-outBits%8) % 8))

		return new this.constructor(outArr, outBits)


	sliceBytes: (a, b) ->
		return new this.constructor(@array.slice(a,b), (b-a)*8)

	toString: (radix=2) ->
		l = radix_width(radix)
		((0x100 | i).toString(radix).slice(-8/l) for i in @array).join('').slice(-@length/l)

	toHexString: -> @toString(16)

	equal: (other) ->
		return false unless other instanceof this.constructor
		return false unless @length == other.length
		for i in [0..@array.length]
			return false unless @array[i] == other.array[i]
		return true

	and: (other) ->
		return false unless @length == other.length
		out = new Uint8Array(@array.length)
		for i in [0..@array.length]
			out[i] = @array[i] & other.array[i]
		return new this.constructor(out, @length)

	or: (other) ->
		return false unless @length == other.length
		out = new Uint8Array(@array.length)
		for i in [0..@array.length]
			out[i] = @array[i] | other.array[i]
		return new this.constructor(out, @length)

	xor: (other) ->
		return false unless @length == other.length
		out = new Uint8Array(@array.length)
		for i in [0..@array.length]
			out[i] = @array[i] ^ other.array[i]
		return new this.constructor(out, @length)

	@fromByte: (b, l=8) ->
		return new this(new Uint8Array([b]), l)

	@fromInt: (num, bits)->
		a = new Uint8Array(Math.ceil(bits/8))
		for i in [a.length-1..0]
			a[i] = num&0xff
			num = num >> 8
		if bits%8 != 0 then a[0] &= 0xff >> (8-bits%8) # mask unused bits
		new this(a, bits)

	@fromBuffer: (data) ->
		return new this(new Uint8Array(data), data.length*8)

	@fromString: (b, radix=2) ->
		l = radix_width(radix)
		bits = b.length*l
		out = new Uint8Array(Math.ceil(bits/8))
		i = out.length-1
		x = b.length
		while i >= 0
			x2 = x-8/l
			out[i--] = parseInt(b.slice(Math.max(x2, 0), x), radix)
			x = x2
		return new this(out, bits)

	@join: (arrs) ->
		outBits = 0
		for i in arrs
			outBits += i.length

		outBytes = Math.ceil(outBits/8)
		outArr = new Uint8Array(outBytes)

		c = 0
		for arr in arrs
			c += arr.length
			i = outBytes - Math.ceil((outBits-c+1)/8) - arr.array.length + 1
			shift = (outBits - c) % 8
			for byte in arr.array
				outArr[i-1] |= byte >> (8-shift)
				outArr[i  ] |= byte << shift
				#console.log(c, pb(byte), shift, pb(byte >> (8-shift)), pb(byte << shift), i, pb(outArr[i]))
				i+=1

		return new this(outArr, outBits)
