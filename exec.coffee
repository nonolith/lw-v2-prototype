# Execution of finite state machines.

_ = require 'underscore'
{isValue} = nodes = require './nodes'
{ASTtoFA, endStep, FABuilder} = require './compile'
{MainContext} = require './context'

# An **Exec** instance tracks the state of a thread of finite automata
# execution. Its state includes the current time along that execution path and
# state of repeat loops.

# Backends can tie additional state by adding immutable data to properties to
# the `.data` property that will be tracked along with the Exec flow. An
# example would be an input buffer position.
@Exec = class Exec
	constructor: (@time, @context, @lastTimerContext=null, @data={}) ->
		@inLoop = false
		@next = null
		@fail = null
		@done = null

	incTime: (t) ->
		@time += t

	# Fork a new **Exec** off of this one for a new thread or speculative
	# execution.
	clone: (cl=@constructor) ->
		new cl(@time, @context, @lastTimerContext, _.extend({}, @data))

	cloneGenerate: () -> @clone(GenerateExec)

	# Merge the state of another exec used for speculative execution back into
	# this one.
	restore: (other) ->
		@time = other.time
		_.extend(@data, other.data)

	_runStep: (step) ->
		#console.log 'run', step.address, step.constructor.name
		step.run(this)

	# Run the finite automata beginning with `startNode`.
	# `cb` is called once it completes by reaching endStep.
	run: (startNode, cb) ->
		@done = cb if cb
		if @next
			# Means that a different next step was already set. Caller has a bug.
			console.trace "exec.next already set"
		@next = startNode

		# Trampoline to implement tail calls in finite stack space. If called
		# from within another call to `.run()`, return and let the stack unwind
		# back to the other call to run, which will pick up `@next` and continue
		# from there.

		if not @inLoop
			@inLoop = true

			# Call the `.run()` (or `.runGenerate()`) method on the next step.
			# The method will call `exec.run(next)` when it chooses the next
			# FA step to run. If this happens later (e.g. in a callback),
			# `@next` will be null, and this loop will break for now. The `.run()`
			# call in the callback will resume this loop.

			while (n = @next)
				@next = null
				if n is endStep
					@done?(@) 
				else
					@_runStep(n)
			@inLoop = false
		null

# Variant of `Exec` that calls the `.runGenerate()` method of steps instead of
# `.run()`. This is used to run a first pass over the FAs compiled to run
# remotely to send them the necessary down-evaluated values.
@GenerateExec = class GenerateExec extends Exec
	isGenerate: true
	_runStep: (step) ->
		#console.log 'runGenerate', step.address, step.constructor.name
		step.runGenerate(this)

# Create a toplevel context and run a main definition. `vars` are bound to the
# variables of the main block. `cb(context)` is called when complete.
@runMain = (mainDef, vars, cb) ->
	context = new MainContext(mainDef.module(), mainDef, vars)
	rootDeviceSet = context.getDeviceSet()

	start = if rootDeviceSet.length==1 and rootDeviceSet[0].ASTtoFA
		# If there's one device involved, let it compile the whole program
		rootDeviceSet[0].ASTtoFA(context, mainDef.body).start
	else
		ASTtoFA(context, mainDef.body).start

	for i in rootDeviceSet
		i.prepare(start)

	exec = new Exec(0, context)
	exec.run start, ->
		for i in rootDeviceSet
			i.after()

		cb(context, exec.time)

		null

# Print the steps in a finite automaton chain.
@printFA = printFA = (start) ->
	indent = ''
	while start
		if start instanceof EventStep
			console.log("#{indent}Event #{start.eventName}")
		else if start instanceof EnterScopeStep
			console.log("#{indent}Enter Scope #{start.scopeParent.entityDef.name}.#{start.def.name}")
			indent += ' '
		else if start instanceof LeaveScopeStep
			indent = indent.slice(1)
			console.log("#{indent}Leave Scope #{start.scopeParent.entityDef.name}.#{start.def.name}")
		else if start instanceof EnterRepeatStep
			console.log("#{indent}Enter Repeat")
			indent += ' '
		else if start instanceof LeaveRepeatStep
			indent = indent.slice(1)
			console.log("#{indent}Leave Repeat or backwards jump")

		start = start.next
