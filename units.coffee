@units = units = {}
units[u] = {units:v, scale:1} for u, v of {
	# http://physics.nist.gov/Pubs/SP811/sec04.html
	# SI Base units
	"m":	{m: 1}
	"kg":	{kg: 1}
	"s":	{s: 1}
	"A":	{A: 1}
	"K":	{K: 1}
	"mol":	{mol: 1}
	"cd":	{cd: 1}

	# SI derived units
	"deg":	{}
	"rad":	{}
	"Hz":	{s: -1}
	"N":	{m:1, kg:1, s: -2}
	"Pa":	{m: -1, kg:1, s: -2}
	"J":	{m:2, kg:1, s: -2}
	"W":	{m:2, kg:1, s: -3}
	"C":	{s:1, A:1}
	"V":	{m:2, kg:1, s: -3, A: -1}
	"F":	{m: -2, kg: -1, s:4, A:2}
	"Ω":	{m:2, kg:1, s: -3, A: -2}
	"ohm":	{m:2, kg:1, s: -3, A: -2}
	"S":	{m: -2, kg: -1, s:3, A:2}
	"Wb":	{m:2, kg:1, s: -2, A: -1}
	"T":	{kg:1, s: -2, A: -1}
	"H":	{m:2, kg:1, s: -2, A: -2}
	"degC":	{K:1}
	"lm":	{Cd:1}
	"lx":	{m: -2, cd:1}
	"Bq":	{s: -1}
	"Gy":	{m:2, s: -2}
	"Sv":	{m:2, s: -2}
	"kat":	{s: -1, mol:1}
}

@prefixes = prefixes =
	Y:	1e24
	Z:	1e21
	E:	1e18
	P:	1e15
	T:	1e12
	G:	1e9
	M:	1e6
	k:	1e3
	h:	1e2
	da:	1e1
	d:	1e-1
	c:	1e-2
	m:	1e-3
	µ:	1e-6
	u:	1e-6
	n:	1e-9
	p:	1e-12
	f:	1e-15
	a:	1e-18
	z:	1e-21
	y:	1e-24

@dimensionless = {units:{}, scale:1}

@equal = equal = (a, b) ->
	for u, c of a.units
		return false if (b.units[u] or 0) != c
	for u, c of b.units
		return false if (a.units[u] or 0) != c
	return true

@multiply = multiply = (a, b, s=1) ->
	u = {}
	u[k] = c for k, c of a.units
	u[k] = c*s + (u[k] or 0) for k, c of b.units
	console.assert(a.scale)
	console.assert(b.scale)
	{units:u, scale:a.scale*b.scale}

@scale = scale = (unit, factor, symbol) ->
	{units:unit.units, scale:unit.scale*factor, symbol} if unit and factor?

@parse = (u) ->
	units[u] or scale(units[u.slice(1)], prefixes[u[0]], u)
